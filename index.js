const phones = [
    {
        id: 1,
        name: 'iPhone 5',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',
        price: 2000000,
    },
    {
        id: 2,
        name: 'iPhone 6',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',       
        price: 13000000,
    },
    {
        id: 3,
        name: 'iPhone 7',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 8999999,
    },
    {
        id: 4,
        name: 'iPhone 8',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 12500000,
    },
    {
        id: 5,
        name: 'iPhone 8+',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 8500000,
    },
    {
        id: 6,
        name: 'iPhone X',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 20000000,
    },
    {
        id: 7,
        name: 'iPhone 11',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 3000000,
    },
    {
        id: 8,
        name: 'iPhone12',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 5500000,
    },
    {
        id: 9,
        name: 'iPhone 13',
        description: '64 GB ROM, 14.73 cm (5.8 inch) Super Retina HD Display 12MP ',        
        image: './img/product.png',        
        price: 13000000,
    },
]

let cart = {};

const modal = document.getElementById('modal-cart');

const formatRupiah = (angka) => "Rp " + angka.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

const renderItems = () => {
    const itemsContainer = document.getElementById('items-container');
    itemsContainer.innerHTML = '';
    phones.forEach(phone => {
        const item = document.createElement('div');
        item.classList.add('d-flex', 'flex-column');
        item.innerHTML = `
            <img src="${phone.image}" class="object-contain" style="height: 240px" alt="${phone.name}">
            <h1 class="fs-4">${phone.name}</h1>
            <span class="text-secondary text-sm">${phone.description}</span>
            <span>${formatRupiah(phone.price)}</span>
            ${
                //if the phone is in the cart, it will show the counter add/remove buttons
                cart[phone.id] ? `<div class="btn-group align-self-start mt-2" role="group">
                    <button type="button" class="btn btn-primary" onclick="removeItem(${phone.id})";>-</button>
                    <button type="button" class="btn btn-outline-primary" disabled>${cart[phone.id]}</button>
                    <button type="button" class="btn btn-primary" onclick="addItem(${phone.id})";>+</button>
                </div>`
                : `<button type="button" class="btn btn-primary align-self-start mt-2" onclick="addItem(${phone.id});">Beli</button>`
            }
        `;
        itemsContainer.appendChild(item);
    })
}

//handles the add item button
const addItem = (id) => {
    //adds item to cart
    cart[id] = cart[id] ? cart[id] + 1 : 1;
    updateCartCount();
    renderItems();
    saveToStorage()
}

const removeItem = (id) => {
    //removes item from cart
    if (!cart[id]) return; //prevent negative values
    cart[id]--;
    if (cart[id] === 0) delete cart[id];
    updateCartCount();
    renderItems();
    saveToStorage()
}

//updates the cart count to the DOM
const updateCartCount = () => {
    const sum = Object.values(cart).reduce((acc, curr) => acc + curr, 0);
    const cartCount = document.getElementById('cart-counter');
    if (sum) {
        cartCount.classList.remove('d-none');
        cartCount.innerHTML = sum;
    } else {
        cartCount.classList.add('d-none');
    }
}

const renderCart = () => {

    const total = Object.keys(cart).reduce((acc, curr) => acc + (cart[curr] * phones.find(phone => phone.id == curr).price), 0);

    const cartContainer = document.getElementById('cart-container');
    cartContainer.innerHTML = '';
    //loops through the cart to render the items
    Object.keys(cart).forEach(id => {
        const item = document.createElement('div');
        const phone = phones.find(phone => phone.id == id);
        item.classList.add('d-flex', 'justify-content-between', 'align-items-center');
        item.innerHTML = `
            <div class="d-flex align-items-center">
                <img src="${phone.image}" class="object-contain" style="height: 60px" alt="${phone.name}">
                <h1 class="fs-5 mb-0">${phone.name}</h1>
                <span class="fs-6 text-secondary">&nbsp;(${cart[id]} x)</span>
            </div>
            <p class="text-success text-right fw-bold mb-0">${formatRupiah(phone.price*cart[id])}</p>
        `;
        cartContainer.appendChild(item);
    })
    document.getElementById('total-price').innerHTML = formatRupiah(total);
}

const saveToStorage = () => {
    localStorage.setItem('cart', JSON.stringify(cart));
}

modal.addEventListener('show.bs.modal', renderCart); //when the modal is shown, render the cart

//initialize the app
(() => {
    if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'));
    }
    renderItems();
    updateCartCount();
})()
